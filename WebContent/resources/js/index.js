var index = new Vue({
    vuetify: new Vuetify(),
	  el:"#inicio",
    data: () => ({
        dialogo: false,
        dialogoDeletar: false,
        cabecalho: [
          {
            text: 'Nome',
            align: 'start',
            value: 'nome',
          },
          {
            text: 'Setor',
            value: 'setor.nome',
          },
          {
            text: 'E-mail',
            value: 'email',
          },
          {
            text: 'Idade',
            value: 'idade',
          },
          {
            text: 'Salário',
            value: 'salario',
          },
          { text: 'Actions', value: 'actions', sortable: false },
        ],
        listaFuncionarios: [],
        listaSetor:{
            id:"",
            nome:""
        },
        indexEditar: -1,
        itemEditado: [],
        itemPadrao: {
          nome: '',
          email: "",
          setor: "",
          idade: "",
          salario: "",

        },
        tela: true,
      }),
    
      computed: {
        formTitle () {
          return this.indexEditar === -1 ? 'Cadastrar Funcionário' : 'Editar Funcionário'
        },
      },
    
      watch: {
        dialogo (val) {
          val || this.fechar()
        },
        dialogoDeletar (val) {
          val || this.fecharDialogoDeletar()
        },
      },
    
      created () {
        this.listarFuncionarios()
        this.listarSetor()
      },
    
      methods: {
        editarItem (item) {
          this.indexEditar = this.listaFuncionarios.indexOf(item)
          this.itemEditado = Object.assign({}, item)
          console.log(item)
          this.dialogo = true
        },
        open() {
          this.$toast.open({
            message: "Test message from Vue",
            type: "success",
            duration: 5000,
            dismissible: true
          })
        },
    
        deletarItem (item) {
          this.indexEditar = this.listaFuncionarios.indexOf(item)
          this.itemEditado = Object.assign({}, item)
          this.dialogoDeletar = true
        },
        mudarTela (){
          console.log(this.tela)
          this.tela = !this.tela
        },
        deletarFuncionario(id){
            axios.delete(`/funcionarios/rs/funcionarios/deletarFuncionario/${id}`)
            .then(response => {
                this.listarFuncionarios();
            }).catch(function (error){
                console.log(error)
            })
        },
        confirmarDelecaoItem () {
          let item = this.listaFuncionarios.splice(this.indexEditar, 1)
          console.log(item)
          this.deletarFuncionario(item[0].id)
          this.fecharDialogoDeletar()
        },
    
        fechar () {
          this.dialogo = false
          this.$nextTick(() => {
            this.itemEditado = Object.assign({}, this.itemPadrao)
            this.indexEditar = -1
          })
        },
    
        fecharDialogoDeletar () {
          this.dialogoDeletar = false
          this.$nextTick(() => {
            this.itemEditado = Object.assign({}, this.itemPadrao)
            this.indexEditar = -1
          })
        },
    
        salvar () {
            if(this.indexEditar === -1){
                let item = Object.assign({}, this.itemEditado)
                let objSetor = []
                this.listaSetor.map((s)=> {if(s.id == item.setor){
                    objSetor = s
                }})
                item.setor = objSetor
                item.idade = parseInt( item.idade)
                item.salario = parseFloat( item.salario)
                console.log(item)
                this.cadastrarFuncionario(item)
            }else{
                let item = Object.assign(this.listaFuncionarios[this.indexEditar], this.itemEditado)
                if(item.setor.nome === undefined){
                    let objSetor = []
                    this.listaSetor.map((s)=> {if(s.id == item.setor){
                        objSetor = s
                    }})
                    item.setor = objSetor
                }
                this.atualizarFuncionario(item)
            }
           
            this.fechar()
        },
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {
                vm.listaFuncionarios = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os funcionário.");
			}).finally(function() {
			});
		},
        cadastrarFuncionario: async function(objFuncionario){
			const vm = this;
			await axios.post("/funcionarios/rs/funcionarios/cadastrarFuncionario", objFuncionario)
			.then(response => {
                this.listarFuncionarios();
			}).catch(function (error) {
                console.log(error)
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os funcionário.");
			}).finally(function() {
			});
		},
        atualizarFuncionario: function(objFuncionario){
			const vm = this;
            console.log(objFuncionario)
			axios.put(`/funcionarios/rs/funcionarios/atualizarFuncionario/${objFuncionario.id}`, objFuncionario)
			.then(response => {
                console.log(response.data)
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível atualizar o funcionário.");
			}).finally(function() {
			});
		},
        listarSetor: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setor/listarSetor")
			.then(response => {
                console.log(response.data)
                vm.listaSetor = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os setores.");
			}).finally(function() {
			});
		},
       
      },
    })
