var setor = new Vue({
    vuetify: new Vuetify(),
	el:"#inicioSetor",
    data: () => ({
        dialogo: false,
        dialogoDeletar: false,
        cabecalho: [
          {
            text: 'Nome',
            align: 'start',
            value: 'nome',
          },
          { text: 'Id', value: 'id', align: ' d-none' },
          { text: 'Actions',  align: 'center',value: 'actions', sortable: false },
        ],
        listaSetor: [],
        indexEditar: -1,
        itemEditado: [],
        itemPadrao: {
          nome: '',
        },
        tela:false,
      }),
    
      computed: {
        formTitle () {
          return this.indexEditar === -1 ? 'Cadastrar Setor' : 'Editar Setor'
        },
      },
    
      watch: {
        dialogo (val) {
          val || this.fechar()
        },
        dialogoDeletar (val) {
          val || this.fecharDialogoDeletar()
        },
      },
    
      created () {
        this.listarSetor()
      },
    
      methods: {
        editarItem (item) {
          console.log(item)
          this.indexEditar = this.listaSetor.indexOf(item)
          this.itemEditado = Object.assign({}, item)
          this.dialogo = true
        },
    
        deletarItem (item) {
          this.indexEditar = this.listaSetor.indexOf(item)
          this.itemEditado = Object.assign({}, item)
          this.dialogoDeletar = true
        },
        deletarSetor(id){
            console.log(id)
            axios.delete(`/funcionarios/rs/setor/deletarSetor/${id}`)
            .then(response => {
                console.log(response)
                this.listarSetor();
            }).catch(function (error){
                console.log(error)
            })
        },
        confirmarDelecaoItem () {
          let item = this.listaSetor.splice(this.indexEditar, 1)
          this.deletarSetor(item[0].id)
          this.fecharDialogoDeletar()
        },
    
        fechar () {
          this.dialogo = false
          this.$nextTick(() => {
            this.itemEditado = Object.assign({}, this.itemPadrao)
            this.indexEditar = -1
          })
        },
    
        fecharDialogoDeletar () {
          this.dialogoDeletar = false
          this.$nextTick(() => {
            this.itemEditado = Object.assign({}, this.itemPadrao)
            this.indexEditar = -1
          })
        },
    
        salvar () {
            console.log(this.itemEditado)
            if(this.indexEditar === -1){
                let item = Object.assign({}, this.itemEditado)
                this.cadastrarSetor(item)
            }else{
                let item = Object.assign(this.listaSetor[this.indexEditar], this.itemEditado)
                this.atualizarSetor(item)
            }
           
            this.fechar()
        },
        cadastrarSetor: async function(objSetor){
			const vm = this;
			await axios.post("/funcionarios/rs/setor/cadastrarSetor", objSetor)
			.then(response => {
                this.listarSetor();
			}).catch(function (error) {
                console.log(error)
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os funcionário.");
			}).finally(function() {
			});
		},
        atualizarSetor: function(objSetor){
			const vm = this;
            console.log(objSetor)
			axios.put(`/funcionarios/rs/setor/atualizarSetor/${objSetor.id}`, objSetor)
			.then(response => {
                console.log(response.data)
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível atualizar o funcionário.");
			}).finally(function() {
			});
		},
        listarSetor: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setor/listarSetor")
			.then(response => {
                console.log(response.data)
                vm.listaSetor = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar os setores.");
			}).finally(function() {
			});
		},
       
      },
    })
