
# processo-seletivo-20

# Passo a passo para rodar o projeto 

1. Baixe e instale o MySql:

2. Abra o terminal e execute as seguintes linhas:

	mysql -u root -p
		
	create database funcionarios_prova;

3. Conecte o banco criado com o dbeaver para visualizar as tabelas existentes:

	1. DataBase: MySQL

	2. Server Host: localhost

	3. Porta: 3306

	4. DataBase: funcionarios_prova

	5. Username: root

	6. Password: root
	
4. Realize o clone do projeto 

	git clone https://gitlab.com/joaocarlossm/processo-seletivo-20.git

5. Abra a pasta do projeto no eclipse

6. Em seguida instale o apache-tomcat

7. Execute o arquivo com.hepta.funcionarios.HibernateUtil para criar o bacno.

8. Execute com o Tomcat:

9. URL de acesso:
	http://localhost:8080/funcionarios/	

10. Passos para realizar o teste:

	OBS: Para realizar o teste de funcionário, primeiramente deve haver um setor cadastrado no banco.

	O primeiro teste é o de criação, persistindo um novo funcionário (ou setor dependendo do teste), assim ao listar um funcionário/setor ira encontrar o que foi persistido e os já existentes no banco (caso houver), prosseguindo com a ordem de execução dos testes buscando por id, atualizando e por fim deletando funcionário/setor criado, para não impactar o banco.
