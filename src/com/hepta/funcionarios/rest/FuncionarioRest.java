package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.service.FuncionarioService;

@Path("/funcionarios")
public class FuncionarioRest {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private FuncionarioService service;

    public FuncionarioRest() {
        service = new FuncionarioService();
    }

    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Adiciona novo Funcionario
     * 
     * @param Funcionario: Novo Funcionario
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @Path("/cadastrarFuncionario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response criarFuncionario(Funcionario funcionario) {
  	  try {
  		  System.out.println(funcionario.getNome());
            service.criarFuncionario(funcionario);
            return Response.status(Status.OK).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Lista todos os Funcionarios
     * 
     * @return response 200 (OK) - Conseguiu listar
     */
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response listarFuncionarios() {
        List<Funcionario> funcionarios = new ArrayList<>();
        try {
            funcionarios = service.listarFuncionarios();
            GenericEntity<List<Funcionario>> entity = new GenericEntity<List<Funcionario>>(funcionarios) {
            };
            return Response.status(Status.OK).entity(entity).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar funcionarios").build();
        }

      
    }

    /**
     * Atualiza um Funcionario
     * 
     * @param id:          id do Funcionario
     * @param Funcionario: Funcionario atualizado
     * @return response 200 (OK) - Conseguiu atualizar
     */
    @Path("/atualizarFuncionario/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response atualizarFuncionario(@PathParam("id") Integer id, Funcionario funcionario) {
    	Funcionario funcionarioAtualizado = new Funcionario();
    	  try {
              funcionarioAtualizado = service.atualizarFuncionario(id, funcionario);
    		  System.out.println(id);
              GenericEntity<Funcionario> entity = new GenericEntity<Funcionario>(funcionarioAtualizado) {
              };
              return Response.status(Status.OK).entity(entity).build();
          } catch (Exception e) {
              return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao atualizar funcionarios").build();
          }
    }

    /**
     * Remove um Funcionario
     * 
     * @param id: id do Funcionario
     * @return response 200 (OK) - Conseguiu remover
     */
    @Path("/deletar-funcionario/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response deletarFuncionario(@PathParam("id") Integer id) {
    	try {
			service.deletarFuncionario(id);
			return Response.status(Status.OK).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao deletar funcionarios").build();
		}
    }

    /**
     * Métodos simples apenas para testar o REST
     * @return
     */
    @Path("/teste")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String TesteJersey() {
        return "Funcionando.";
    }

}
