package com.hepta.funcionarios.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.service.SetorService;

@Path("/setor")
public class SetorRest {

    @Context
    private HttpServletRequest request;

    @Context
    private HttpServletResponse response;

    private SetorService service;

    public SetorRest() {
        service = new SetorService();
    }

    protected void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Adiciona novo Setor
     * 
     * @param Setor: Novo Setor
     * @return response 200 (OK) - Conseguiu adicionar
     */
    @Path("/cadastrarSetor/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response criarSetor(Setor setor) {
  	  try {
            service.criarSetor(setor);
            return Response.status(Status.OK).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao criar Setor").build();
        }
    }

    /**
     * Lista todos os Setor
     * 
     * @return response 200 (OK) - Conseguiu listar
     */
    @Path("/listarSetor")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response listarSetor() {
        List<Setor> setor = new ArrayList<>();
        try {
        	setor = service.listarSetor();
            GenericEntity<List<Setor>> entity = new GenericEntity<List<Setor>>(setor) {
            };
            return Response.status(Status.OK).entity(entity).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Erro ao buscar Setor").build();
        }

      
    }

    /**
     * Atualiza um Setor
     * 
     * @param id:          id do Setor
     * @param Setor: Setor atualizado
     * @return response 200 (OK) - Conseguiu atualizar
     */
    @Path("/atualizarSetor/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response atualizarSetor(@PathParam("id") Integer id, Setor setor) {
    	Setor setorAtualizado = new Setor();
    	  try {
    		  setorAtualizado = service.atualizarSetor(id, setor);
              GenericEntity<Setor> entity = new GenericEntity<Setor>(setorAtualizado) {
              };
              return Response.status(Status.OK).entity(entity).build();
          } catch (Exception e) {
              return Response.status(Status.INTERNAL_SERVER_ERROR).build();
          }
    }

    /**
     * Remove um Setor
     * 
     * @param id: id do Setor
     * @return response 200 (OK) - Conseguiu remover
     */
    @Path("/deletarSetor/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @DELETE
    public Response deletarSetor(@PathParam("id") Integer id) {
    	try {
			service.deletarSetor(id);
			return Response.status(Status.OK).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
    }

    /**
     * Métodos simples apenas para testar o REST
     * @return
     */
    @Path("/teste")
    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String TesteJersey() {
        return "Funcionando.";
    }

}
