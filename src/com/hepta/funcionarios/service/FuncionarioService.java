package com.hepta.funcionarios.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

public class FuncionarioService {

	private FuncionarioDAO dao;

	public FuncionarioService() {
		dao = new FuncionarioDAO();
	}

	public List<Funcionario> listarFuncionarios() {
		List<Funcionario> funcionarios = new ArrayList<>();
		try {
			funcionarios = dao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionarios;
	}

	public void deletarFuncionario(Integer id) {
		try {
			dao.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Funcionario buscarFuncionarioPorID(Integer id) {
		Funcionario funcionario = new Funcionario();
		try {
			funcionario = dao.find(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionario;
	}

	public void criarFuncionario(Funcionario funcionario) {
		try {
			dao.save(funcionario);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Funcionario atualizarFuncionario(Integer id, Funcionario funcionario) {
		Funcionario funcionarioAntigo = new Funcionario();
		try {
			funcionarioAntigo = dao.find(id);
    		funcionarioAntigo = funcionario;
			dao.update(funcionarioAntigo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return funcionarioAntigo;
	}
	
	
	
}
