package com.hepta.funcionarios.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.SetorDAO;

public class SetorService {

	private SetorDAO dao;

	public SetorService() {
		dao = new SetorDAO();
	}

	public List<Setor> listarSetor() {
		List<Setor> setor = new ArrayList<>();
		try {
			setor = dao.getAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return setor;
	}

	public void deletarSetor(Integer id) {
		try {
			dao.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Setor buscarSetorPorID(Integer id) {
		Setor setor = new Setor();
		try {
			setor = dao.find(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return setor;
	}

	public void criarSetor(Setor setor) {
		try {
			dao.save(setor);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Setor atualizarSetor(Integer id, Setor setor) {
		Setor setorAntigo = new Setor();
		try {
			setorAntigo = dao.find(id);
    		setorAntigo = setor;
			dao.update(setorAntigo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return setorAntigo;
	}
	
	
	
}
