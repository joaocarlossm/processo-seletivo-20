package com.hepta.funcionarios.service;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.hepta.funcionarios.entity.Setor;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SetorServiceTest {

	SetorService service;

	static Setor setor;

	public SetorServiceTest() {
		service = new SetorService();
	}

	@BeforeAll
	static void criarSetor() throws Exception {
		setor = new Setor();
		setor.setNome("Suporte");
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@Order(3)
	@Test
	void testBuscarSetorPorID() {
		Setor setorBuscado = service.buscarSetorPorID(setor.getId());
		assert (Objects.nonNull(setorBuscado));
	}

	@Order(2)
	@Test
	void testListarSetor() {
		List<Setor> setor = service.listarSetor();
		for(Setor set: setor) {
			System.out.println(set.getNome());
		}
		assert(!setor.isEmpty());
	}

	@Order(1)
	@Test
	void testCriarSetor() {
		service.criarSetor(setor);
		Setor newsetor = service.buscarSetorPorID(setor.getId());
		Assertions.assertEquals("Suporte", newsetor.getNome());
	}

	@Order(4)
	@Test
	void testAtualizarSetor() {
		setor.setNome("Banco");
		service.atualizarSetor(setor.getId(), setor);
		Setor newsetor = service.buscarSetorPorID(setor.getId());
		Assertions.assertEquals("Banco", newsetor.getNome());
	}

	@Order(5)
	@Test
	void testDeletarSetor() {
		service.deletarSetor(setor.getId());
		Setor setorBuscado = service.buscarSetorPorID(setor.getId());
		assert (Objects.isNull(setorBuscado));
	}
	
}
