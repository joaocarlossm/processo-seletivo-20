package com.hepta.funcionarios.service;

import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FuncionarioServiceTest {

	FuncionarioService service;

	static Funcionario funcionario;

	public FuncionarioServiceTest() {
		service = new FuncionarioService();
	}

	@BeforeAll
	static void criarFuncionario() throws Exception {
		funcionario = new Funcionario();
		funcionario.setNome("Joao");
		funcionario.setEmail("jc@gmail.com");
		funcionario.setIdade(18);
		funcionario.setSalario(12.600);
		Setor setor = new Setor();
		setor.setId(1);
		setor.setNome("DESENVOLVIMENTO");
		funcionario.setSetor(setor);
	}

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@Order(3)
	@Test
	void testBuscarFuncionarioPorID() {
		Funcionario funcionarioBuscado = service.buscarFuncionarioPorID(funcionario.getId());
		assert (Objects.nonNull(funcionarioBuscado));
	}

	@Order(2)
	@Test
	void testListarFuncionario() {
		List<Funcionario> funcionarios = service.listarFuncionarios();
		assert (Objects.nonNull(funcionarios));
	}

	@Order(1)
	@Test
	void testCriarFuncionario() {
		service.criarFuncionario(funcionario);
		Funcionario newfuncionario = service.buscarFuncionarioPorID(funcionario.getId());
		Assertions.assertEquals("Joao", newfuncionario.getNome());
	}

	@Order(4)
	@Test
	void testAtualizarFuncionario() {
		funcionario.setNome("Joao Teste");
		service.atualizarFuncionario(funcionario.getId(), funcionario);
		Funcionario newfuncionario = service.buscarFuncionarioPorID(funcionario.getId());
		Assertions.assertEquals("Joao Teste", newfuncionario.getNome());
	}

	@Order(5)
	@Test
	void testDeletarFuncionario() {
		service.deletarFuncionario(funcionario.getId());
		Funcionario funcionarioBuscado = service.buscarFuncionarioPorID(funcionario.getId());
		assert (Objects.isNull(funcionarioBuscado));
	}
	
}
